@extends('layout.master')

@section('judul')
    Edit Data Cast {{$cast->name}}
@endsection

@section('content')

<div>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="title">Nama Cast</label>
            <input type="text" class="form-control" value="{{$cast->name}}" name="name" id="title" placeholder="Masukkan Nama">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Umur Cast</label>
            <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" id="title" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Bio Cast</label>
            <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>

@endsection