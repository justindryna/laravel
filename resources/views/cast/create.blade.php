@extends('layout.master')

@section('judul')
    Tambah Data Cast
@endsection

@section('content')

<div>
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">Nama Cast</label>
            <input type="text" class="form-control" name="name" id="title" placeholder="Masukkan Nama">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Umur Cast</label>
            <input type="text" class="form-control" name="umur" id="title" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Bio Cast</label>
            <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>

@endsection