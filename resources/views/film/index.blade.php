@extends('layout.master')

@section('judul')
    Halaman List Film
@endsection

@section('content')

<div class="row">
    @foreach ($film as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('poster/' . $item->poster)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h4>{{$item->judul}} ({{$item->tahun}})</h4>
                <p class="card-text">{{Str::limit($item->ringkasan, 50)}}
                <a href="/film/{{$item->id}}" class="btn btn-success btn-sm">Readmore</a>
                </p>
            </div>
        </div>
    </div>
    @endforeach
</div>



@endsection