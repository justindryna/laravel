@extends('layout.master')

@section('judul')
    Halaman Detail Film {{$film->judul}}
@endsection

@section('content')

<div class="row">
    <div class="col-6">
        <div class="card">
            <img src="{{asset('poster/' . $film->poster)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h4>{{$film->judul}} ({{$film->tahun}})</h4>
                <p class="card-text"> {{($film->ringkasan)}} </p>
                <a href="/film" class="btn btn-success btn-sm">Kembali
                </a>
            </div>
        </div>
    </div>
    <div class="col-6">
        
    </div>
</div>

@endsection