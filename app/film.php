<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    protected $table = 'film';

    protected $fillable = ['judul', 'ringkasan', 'poster', 'tahun', 'genre_id'];
}
