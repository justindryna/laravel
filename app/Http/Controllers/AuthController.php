<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata()
    {
        return view('tugas1.form');
    }
    public function kirim(Request $request)
    {
        $name = $request['nama_depan'];
        $name2 = $request['nama_belakang'];
        $bio = $request['address'];
        $gender = $request['jk'];
        $status = $request['wn'];

        return view('tugas1.welcome', compact('name','name2','bio','gender'));
    }
}
